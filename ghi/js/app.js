function createCard(name, description, pictureUrl, starts, ends, location_name) {
    return `
    <div class="col-4">
      <div class="card shadow-lg m-2">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location_name}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${starts.getMonth() + 1}/${starts.getDate()}/${starts.getFullYear()} -${ends.getMonth() + 1}/${ends.getDate()}/${ends.getFullYear()}
        </div>
      </div>
    </div>
    `;
  }
window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = new Date(details.conference.starts)
          const ends = new Date(details.conference.ends)
          const location_name = details.conference.location.name
          const html = createCard(name, description, pictureUrl, starts, ends, location_name);
          const column = document.querySelector('.row');
          column.innerHTML += html;
        }
      }

    }
  } catch (e) {
    // Figure out what to do if an error is raised
  }

});
